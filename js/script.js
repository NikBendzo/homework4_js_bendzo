"use strict";

function calc(operation, num1, num2) {
    const validOperation = ['+', '-', '*', '/'];
    
    while (!validOperation.includes(operation)) {
        operation = prompt("Введіть операцію (+, -, *, /):");
    }

    if (isNaN(num1)) {
        do {
            num1 = parseFloat(prompt("Введіть перше число:"));
        } while (isNaN(num1));
    }

    if (isNaN(num2)) {
        do {
            num2 = parseFloat(prompt("Введіть друге число:"));
        } while (isNaN(num2));
    }

    switch (operation) {
        case "+":
            return num1 + num2;
        case "-":
            return num1 - num2;
        case "*":
            return num1 * num2;
        case "/":
            if (num2 === 0) {
                return "Помилка: ділення на нуль";
            }
            return num1 / num2;
    }
}

const result = calc();
console.log(result);